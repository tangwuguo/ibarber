package com.example.service;

import com.example.entity.BuyProduct;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mapper.BuyProductMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class BuyProductService extends ServiceImpl<BuyProductMapper, BuyProduct> {

    @Resource
    private BuyProductMapper buyProductMapper;

}