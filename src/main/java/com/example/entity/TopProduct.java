package com.example.entity;

import lombok.Data;

@Data
public class TopProduct {
    private String name;
    private Integer value;
}
