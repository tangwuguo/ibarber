package com.example.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

@TableName("buyservice")
public class BuyService extends Model<BuyService> {
    /**
      * 主键
      */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
      * 会员id 
      */
    private Integer vId;

    /**
      * 服务id 
      */
    private Integer sId;

    /**
      * 员工id 
      */
    private Integer staffid;

    /**
      * 时间 
      */
    private Date time;

    public Integer getVId() {
        return vId;
    }

    public void setVId(Integer vId) {
         this.vId = vId;
    }

    public Integer getSId() {
        return sId;
    }

    public void setSId(Integer sId) {
         this.sId = sId;
    }

    public Integer getStaffid() {
        return staffid;
    }

    public void setStaffid(Integer staffid) {
         this.staffid = staffid;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
         this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
         this.id = id;
    }

}