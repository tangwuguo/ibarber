package com.example.entity;

import lombok.Data;

@Data
public class RecentSale {
    private String date;
    private Integer money;
}
