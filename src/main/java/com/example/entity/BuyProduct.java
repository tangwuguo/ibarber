package com.example.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

@TableName("buyproduct")
public class BuyProduct extends Model<BuyProduct> {
    /**
      * 主键
      */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
      * 会员ID 
      */
    private Integer vId;

    /**
      * 商品ID 
      */
    private Integer pId;

    /**
      * 时间 
      */
    private Date time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
         this.id = id;
    }

    public Integer getVId() {
        return vId;
    }

    public void setVId(Integer vId) {
         this.vId = vId;
    }

    public Integer getPId() {
        return pId;
    }

    public void setPId(Integer pId) {
         this.pId = pId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
         this.time = time;
    }

}