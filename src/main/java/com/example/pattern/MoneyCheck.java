package com.example.pattern;

public class MoneyCheck extends BuyCheck {

    @Override
    public void filterProcess() {
        System.out.println("判断余额是否足够");
    }

    public BuyCheck getChecker() {
        RepertoryCheck repertoryCheck = new RepertoryCheck();
        repertoryCheck.setNextFilter(new Buy());
        this.setNextFilter(repertoryCheck);
        return this;
    }
}
