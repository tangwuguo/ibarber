package com.example.pattern;

public class StaticsMsg extends Msg {
    public int allSale;
    public int productCount;
    public int serviceCount;
    public int vipCount;

    public StaticsMsg(int allSale, int productCount, int serviceCount, int vipCount) {
        this.msgType = "产生新订单";
        this.allSale = allSale;
        this.productCount = productCount;
        this.serviceCount = serviceCount;
        this.vipCount = vipCount;
    }

    @Override
    public String toString() {
        return "StaticsMsg{" +
                "allSale=" + allSale +
                ", productCount=" + productCount +
                ", serviceCount=" + serviceCount +
                ", vipCount=" + vipCount +
                '}';
    }
}
