package com.example.pattern;

import com.example.entity.User;

import java.util.HashSet;
import java.util.Set;

public class PushMsg {
    private static PushMsg pusher = new PushMsg();
    private Set<User> observers = new HashSet<User>();

    private PushMsg() { }

    public static PushMsg getPusher() {
        return pusher;
    }

    public void attach(User u) {
        observers.add(u);
    }

    public void detach(User u) {
        observers.remove(u);
    }

    //    通知观察者-推送消息
    public void notifyObs() {
        Msg message = new StaticsMsg(0, 0, 0, 0);
        for (User u : observers) {
            u.update(message);
        }
    }
}
