package com.example.pattern;

import com.example.entity.User;

public class MsgClient {
    public static void main(String[] args) {
        User u1 = new User();
        User u2 = new User();
        PushMsg pusher = PushMsg.getPusher();
        pusher.attach(u1);
        pusher.attach(u2);
        pusher.notifyObs();

    }
}
