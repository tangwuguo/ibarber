package com.example.pattern;

public class BuyCheck {

    protected BuyCheck nextFilter;

    public void setNextFilter(BuyCheck buyCheck) {
        this.nextFilter = buyCheck;
    }

    public void filterProcess() {
    }

    public void handleData() {
        filterProcess();
        if (nextFilter != null) {
            nextFilter.handleData();
        }
    }

}
