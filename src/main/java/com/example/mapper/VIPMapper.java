package com.example.mapper;

import com.example.entity.VIP;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

public interface VIPMapper extends BaseMapper<VIP> {

    @Select("SELECT count(id) FROM vip ")
    Integer findVipSum();
}