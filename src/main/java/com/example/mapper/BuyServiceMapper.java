package com.example.mapper;

import com.example.entity.BuyService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.RecentSale;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface BuyServiceMapper extends BaseMapper<BuyService> {
    @Select("SELECT SUM(s.price) FROM buyservice bs, service s WHERE bs.s_id=s.id AND TO_DAYS(bs.time)=TO_DAYS(now())")
    Integer findSales();

    @Select("SELECT COUNT(id) FROM buyservice as b WHERE TO_DAYS(b.time)=TO_DAYS(now())")
    Integer findServiceSalesVolume();

    @Select("SELECT SUM(s.price) 'money',date_format(bs.time, '%Y-%m-%d') 'date' \n" +
            "from buyservice AS bs, service AS s\n" +
            "where s.id=bs.s_id \n" +
            "group by date_format(bs.time, '%Y-%m-%d')\n" +
            "ORDER BY date_format(bs.time, '%Y-%m-%d') DESC\n" +
            "LIMIT 5")
    List<RecentSale> findRecentServiceSale();
}