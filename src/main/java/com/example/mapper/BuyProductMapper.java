package com.example.mapper;

import com.example.entity.BuyProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.RecentSale;
import com.example.entity.TopProduct;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface BuyProductMapper extends BaseMapper<BuyProduct> {
    @Select("SELECT SUM(p.price) FROM buyproduct bp, product p WHERE bp.p_id=p.id AND TO_DAYS(bp.time)=TO_DAYS(now())")
    Integer findSale();

    @Select("SELECT COUNT(id) FROM buyproduct as b WHERE TO_DAYS(b.time)=TO_DAYS(now())")
    Integer findProductSalesVolume();

    @Select("SELECT p.`name` 'name',COUNT(p.id) 'value' from buyproduct as b, product as p  where TO_DAYS(NOW())-TO_DAYS(b.time)<=30 and p.id = b.p_id GROUP BY b.p_id ORDER BY COUNT(p.id) DESC LIMIT 5")
    List<TopProduct> findTopProduct();
    @Select("SELECT SUM(p.price) 'money',date_format(bp.time, '%Y-%m-%d') 'date' \n" +
            "from buyproduct as bp, product as p\n" +
            "where p.id=bp.p_id\n" +
            "group by date_format(bp.time, '%Y-%m-%d')\n" +
            "ORDER BY date_format(bp.time, '%Y-%m-%d') DESC\n" +
            "LIMIT 5;")
    List<RecentSale> findRecentProductSale();
}