package com.example.controller;

import com.example.common.Result;
import com.example.entity.BuyProduct;
import com.example.pattern.MoneyCheck;
import com.example.pattern.PushMsg;
import com.example.service.BuyProductService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/buyProduct")
public class BuyProductController {
    @Resource
    private BuyProductService buyProductService;

    @PostMapping
    public Result<?> save(@RequestBody BuyProduct buyProduct) {
        new MoneyCheck().getChecker().handleData();         //检查钱包是否足够、检查库存、、、
        Result<Boolean> success = Result.success(buyProductService.save(buyProduct));
        PushMsg.getPusher().notifyObs();
        return success;

    }

    @PutMapping
    public Result<?> update(@RequestBody BuyProduct buyProduct) {
        new MoneyCheck().getChecker().handleData();
        Result<Boolean> success = Result.success(buyProductService.updateById(buyProduct));
        PushMsg.getPusher().notifyObs();
        return success;

    }

    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Long id) {
        buyProductService.removeById(id);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result<BuyProduct> findById(@PathVariable Long id) {
        return Result.success(buyProductService.getById(id));
    }

    @GetMapping
    public Result<List<BuyProduct>> findAll() {
        return Result.success(buyProductService.list());
    }

    @GetMapping("/page")
    public Result<IPage<BuyProduct>> findPage(@RequestParam(required = false, defaultValue = "") String name,
                                              @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                              @RequestParam(required = false, defaultValue = "10") Integer pageSize) {
        return Result.success(buyProductService.page(new Page<>(pageNum, pageSize), Wrappers.<BuyProduct>lambdaQuery().like(BuyProduct::getId, name)));
    }

}