package com.example.controller;

import com.example.common.Result;
import com.example.entity.BuyService;
import com.example.pattern.PushMsg;
import com.example.service.BuyServiceService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/buyService")
public class BuyServiceController {
    @Resource
     private BuyServiceService buyServiceService;

    @PostMapping
    public Result<?> save(@RequestBody BuyService buyService) {
        Result<Boolean> success = Result.success(buyServiceService.save(buyService));
        PushMsg.getPusher().notifyObs();
        return success;

    }

    @PutMapping
    public Result<?> update(@RequestBody BuyService buyService) {
        Result<Boolean> success = Result.success(buyServiceService.updateById(buyService));
        PushMsg.getPusher().notifyObs();
        return success;

    }

    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Long id) {
        buyServiceService.removeById(id);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result<BuyService> findById(@PathVariable Long id) {
        return Result.success(buyServiceService.getById(id));
    }

    @GetMapping
    public Result<List<BuyService>> findAll() {
        return Result.success(buyServiceService.list());
    }

    @GetMapping("/page")
    public Result<IPage<BuyService>> findPage(@RequestParam(required = false, defaultValue = "") String name,
                                           @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                           @RequestParam(required = false, defaultValue = "10") Integer pageSize) {
        return Result.success(buyServiceService.page(new Page<>(pageNum, pageSize), Wrappers.<BuyService>lambdaQuery().like(BuyService::getId, name)));
    }

}