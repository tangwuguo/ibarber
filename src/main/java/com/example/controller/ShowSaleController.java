package com.example.controller;


import com.example.entity.RecentSale;
import com.example.entity.TopProduct;
import com.example.mapper.BuyProductMapper;
import com.example.mapper.BuyServiceMapper;
import com.example.mapper.VIPMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/showSale")
public class ShowSaleController {
    @Resource
    BuyProductMapper buyProductMapper;

    @Resource
    BuyServiceMapper buyServiceMapper;

    @Resource
    VIPMapper vipMapper;

    /**
     * 查询销售额
     *
     * @return Map
     */
    @GetMapping("/sale")
    public Map<String, Integer> getSale() {
        Map<String, Integer> map = new HashMap<>();
        Integer a = buyProductMapper.findSale();
        Integer b = buyServiceMapper.findSales();
        if (a == null)
            a = 0;
        if (b == null)
            b = 0;
        Integer i = a + b;
        map.put("sale", i);
        return map;
    }

    /**
     * 查询商品销售数
     *
     * @return map
     */
    @GetMapping("/productSaleVolume")
    public Map<String, Integer> getProductVolume() {
        Map<String, Integer> map = new HashMap<>();
        Integer i = buyProductMapper.findProductSalesVolume();
        map.put("productSaleVolume", i);
        return map;
    }

    /**
     * 查询服务销售数
     *
     * @return map
     */
    @GetMapping("/serviceSaleVolume")
    public Map<String, Integer> getServiceVolume() {
        Map<String, Integer> map = new HashMap<>();
        Integer i = buyServiceMapper.findServiceSalesVolume();
        map.put("serviceSaleVolume", i);
        return map;
    }

    /**
     * 查询会员数
     *
     * @return map
     */
    @GetMapping("/getVipSum")
    public Map<String, Integer> getVipSum() {
        Map<String, Integer> map = new HashMap<>();
        Integer i = vipMapper.findVipSum();
        map.put("vipSum", i);
        return map;
    }

    /**
     * 查询销量最高的五个商品
     *
     * @return map
     */
    @GetMapping("/findTopProduct")
    public List<TopProduct> findTopProduct() {
        return buyProductMapper.findTopProduct();
    }

    /**
     * 查询最近销售服务的金额
     *
     * @return map
     */
    @GetMapping("/findRecentServiceSale")
    public List<RecentSale> findRecentServiceSale() {

        return buyServiceMapper.findRecentServiceSale();
    }

    /**
     * 查询最近商品服务的金额
     *
     * @return map
     */
    @GetMapping("/findRecentProductSale")
    public List<RecentSale> findRecentProductSale() {

        return buyProductMapper.findRecentProductSale();
    }

}
