/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80020
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 80020
File Encoding         : 65001

Date: 2021-07-03 22:44:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `buyproduct`
-- ----------------------------
DROP TABLE IF EXISTS `buyproduct`;
CREATE TABLE `buyproduct` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `v_id` int NOT NULL COMMENT '会员ID',
  `p_id` int NOT NULL COMMENT '商品ID',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `v_p_FK` (`v_id`) USING BTREE,
  KEY `v_p_FK_1` (`p_id`) USING BTREE,
  CONSTRAINT `v_p_FK` FOREIGN KEY (`v_id`) REFERENCES `vip` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `v_p_FK_1` FOREIGN KEY (`p_id`) REFERENCES `product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of buyproduct
-- ----------------------------
INSERT INTO `buyproduct` VALUES ('5', '1', '2', '2021-06-30 22:05:24');
INSERT INTO `buyproduct` VALUES ('6', '3', '1', '2021-07-01 15:36:15');
INSERT INTO `buyproduct` VALUES ('7', '4', '1', '2021-07-01 19:58:03');
INSERT INTO `buyproduct` VALUES ('8', '4', '5', '2021-07-01 19:58:43');
INSERT INTO `buyproduct` VALUES ('9', '4', '8', '2021-07-01 19:58:47');
INSERT INTO `buyproduct` VALUES ('10', '2', '3', '2021-07-01 19:58:50');
INSERT INTO `buyproduct` VALUES ('11', '1', '1', '2021-06-29 20:06:09');
INSERT INTO `buyproduct` VALUES ('12', '4', '4', '2021-06-27 20:06:30');
INSERT INTO `buyproduct` VALUES ('13', '2', '4', '2021-06-28 21:04:03');
INSERT INTO `buyproduct` VALUES ('14', '3', '2', '2021-07-02 14:59:54');
INSERT INTO `buyproduct` VALUES ('15', '3', '2', '2021-07-02 15:26:34');

-- ----------------------------
-- Table structure for `buyservice`
-- ----------------------------
DROP TABLE IF EXISTS `buyservice`;
CREATE TABLE `buyservice` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `v_id` int NOT NULL COMMENT '会员id',
  `s_id` int NOT NULL COMMENT '服务id',
  `staffid` int NOT NULL DEFAULT '-1' COMMENT '员工id',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `v_s_FK` (`v_id`) USING BTREE,
  KEY `v_s_FK_1` (`staffid`) USING BTREE,
  KEY `v_s_FK_2` (`s_id`) USING BTREE,
  CONSTRAINT `v_s_FK` FOREIGN KEY (`v_id`) REFERENCES `vip` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `v_s_FK_1` FOREIGN KEY (`staffid`) REFERENCES `staff` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `v_s_FK_2` FOREIGN KEY (`s_id`) REFERENCES `service` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of buyservice
-- ----------------------------
INSERT INTO `buyservice` VALUES ('1', '2', '1', '1', '2021-06-30 22:09:42');
INSERT INTO `buyservice` VALUES ('2', '2', '8', '1', '2021-07-01 15:35:21');
INSERT INTO `buyservice` VALUES ('3', '4', '7', '1', '2021-07-01 15:35:39');
INSERT INTO `buyservice` VALUES ('4', '2', '1', '1', '2021-07-01 19:57:40');
INSERT INTO `buyservice` VALUES ('5', '3', '1', '1', '2021-07-01 19:57:48');
INSERT INTO `buyservice` VALUES ('6', '2', '3', '2', '2021-06-30 20:04:53');
INSERT INTO `buyservice` VALUES ('7', '2', '1', '2', '2021-06-30 20:05:13');
INSERT INTO `buyservice` VALUES ('8', '1', '2', '2', '2021-06-28 20:05:28');
INSERT INTO `buyservice` VALUES ('9', '3', '3', '3', '2021-06-27 20:07:38');
INSERT INTO `buyservice` VALUES ('10', '2', '1', '3', '2021-06-29 20:08:13');
INSERT INTO `buyservice` VALUES ('11', '3', '4', '1', '2021-07-01 22:25:06');
INSERT INTO `buyservice` VALUES ('12', '3', '2', '1', '2021-07-02 15:10:02');
INSERT INTO `buyservice` VALUES ('13', '1', '1', '1', '2021-07-02 15:11:52');
INSERT INTO `buyservice` VALUES ('14', '2', '1', '1', '2021-07-02 15:14:03');
INSERT INTO `buyservice` VALUES ('15', '4', '1', '1', '2021-07-02 15:17:59');
INSERT INTO `buyservice` VALUES ('16', '3', '1', '1', '2021-07-02 15:18:47');

-- ----------------------------
-- Table structure for `charge`
-- ----------------------------
DROP TABLE IF EXISTS `charge`;
CREATE TABLE `charge` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `money` int NOT NULL COMMENT '充值金额',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
  `vid` int NOT NULL COMMENT '用户ID',
  `uname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `charge_FK` (`vid`) USING BTREE,
  CONSTRAINT `charge_FK` FOREIGN KEY (`vid`) REFERENCES `vip` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of charge
-- ----------------------------
INSERT INTO `charge` VALUES ('1', '100', '2021-06-22 00:00:00', '1', '小五');
INSERT INTO `charge` VALUES ('2', '200', '2021-06-18 00:00:00', '2', '迅迅');
INSERT INTO `charge` VALUES ('3', '500', '2021-06-22 00:00:00', '3', '苏吉拉');
INSERT INTO `charge` VALUES ('4', '100', '2021-06-20 00:00:00', '4', '平平');
INSERT INTO `charge` VALUES ('5', '200', '2021-07-01 19:51:19', '4', '平平');
INSERT INTO `charge` VALUES ('8', '200', '2021-07-01 19:53:53', '1', '小五');
INSERT INTO `charge` VALUES ('9', '500', '2021-07-01 19:54:59', '1', '小五');
INSERT INTO `charge` VALUES ('10', '500', '2021-07-01 19:57:13', '4', '平平');
INSERT INTO `charge` VALUES ('11', '700', '2021-07-01 19:59:16', '2', '迅迅');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名',
  `price` float NOT NULL COMMENT '价格',
  `hint` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '介绍',
  `point` int DEFAULT '0' COMMENT '积点',
  `pic` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', 'VS沙宣修护水养洗发水', '65', '持久修护毛躁滋润干枯', '6', 'https://img.alicdn.com/imgextra/i1/725677994/O1CN01AYXE0728vIpYJy4pl_!!725677994-0-sm.jpg_430x430q90.jpg');
INSERT INTO `product` VALUES ('2', '海飞丝洗发水', '64', '怡神去屑 清爽去油', '6', 'https://img.alicdn.com/imgextra/i2/725677994/O1CN01d2I9aL28vIoYXSyiK_!!725677994-0-sm.jpg_430x430q90.jpg');
INSERT INTO `product` VALUES ('3', '阿道夫洗发水', '176', '去屑止痒控油蓬松持久留香', '17', 'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i4/2962070462/O1CN014VMHfv1FHdZ9g5Rmt_!!2962070462.jpg_430x430q90.jpg');
INSERT INTO `product` VALUES ('4', '蜂花护发素', '58', '36年经典 小麦蛋白补充养分', '5', 'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i4/2452362472/O1CN01SsxYdm1U8DiELpq8Y_!!2452362472.jpg_430x430q90.jpg');
INSERT INTO `product` VALUES ('5', '潘婷氨基酸护发素', '49', '每洗1次 强韧1次', '4', 'https://img.alicdn.com/imgextra/i3/725677994/O1CN01pPAUCZ28vIpdNAwus_!!725677994-0-sm.jpg_430x430q90.jpg');
INSERT INTO `product` VALUES ('6', '欧莱雅发油护发精油', '95', '网红力荐 一抹顺滑不油腻', '9', 'https://img.alicdn.com/imgextra/i1/725677994/O1CN01gk0vKM28vIq3EeWBD_!!725677994-0-sm.jpg_430x430q90.jpg');
INSERT INTO `product` VALUES ('8', '杰威尔定型喷雾', '68', '持久定型 一喷一抹 型男利器', '6', 'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i3/756239978/O1CN01on1ZUy2NZycWKDWGt_!!756239978.jpg_430x430q90.jpg');
INSERT INTO `product` VALUES ('9', '欧莱雅男士哑光发泥', '59', '造型入门单品 哑光自然', '5', 'https://img.alicdn.com/imgextra/i3/2786278078/O1CN01xJyOql29XlzMXRanm_!!2786278078.jpg_430x430q90.jpg');

-- ----------------------------
-- Table structure for `service`
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `price` int NOT NULL COMMENT '价格',
  `hint` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '介绍',
  `point` int NOT NULL DEFAULT '0' COMMENT '积点',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES ('1', '理发', '15', null, '1');
INSERT INTO `service` VALUES ('2', '洗吹造型', '28', null, '2');
INSERT INTO `service` VALUES ('3', '时尚烫发', '98', null, '9');
INSERT INTO `service` VALUES ('4', '精品烫发', '188', null, '18');
INSERT INTO `service` VALUES ('5', '时尚染发', '78', null, '7');
INSERT INTO `service` VALUES ('6', '头发护理', '39', null, '3');
INSERT INTO `service` VALUES ('7', '锡纸烫', '128', null, '12');
INSERT INTO `service` VALUES ('8', '大波浪卷发', '128', '', '12');

-- ----------------------------
-- Table structure for `staff`
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `gender` tinyint(1) DEFAULT '1' COMMENT '性别',
  `number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电话',
  `status` int NOT NULL DEFAULT '1' COMMENT '等级',
  `pwd` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '123456',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('1', 'admin', '1', '13873333011', '1', '123456');
INSERT INTO `staff` VALUES ('2', 'Tony', '1', '13988787787', '3', '123456');
INSERT INTO `staff` VALUES ('3', 'Lucy', '0', '13976676776', '2', '123456');

-- ----------------------------
-- Table structure for `t_permission`
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单路径',
  `flag` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '唯一标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='权限菜单表';

-- ----------------------------
-- Records of t_permission
-- ----------------------------
INSERT INTO `t_permission` VALUES ('1', '用户管理', '用户管理', '/page/end/user', 'user');
INSERT INTO `t_permission` VALUES ('2', '角色管理', '角色管理', '/page/end/role.html', 'role');
INSERT INTO `t_permission` VALUES ('3', '权限管理', '权限管理', '/page/end/permission.html', 'permission');
INSERT INTO `t_permission` VALUES ('4', '插件管理', '插件管理', '/page/end/plugins.html', 'plugins');
INSERT INTO `t_permission` VALUES ('18', '聊天室', '聊天室', '/page/end/im.html', 'im');
INSERT INTO `t_permission` VALUES ('26', 'VIP管理', 'VIP管理', '/page/end/vip.html', 'VIP');
INSERT INTO `t_permission` VALUES ('27', '商品管理', '商品管理', '/page/end/product.html', 'product');
INSERT INTO `t_permission` VALUES ('28', '服务项目管理', '服务项目管理', '/page/end/service.html', 'MyService');
INSERT INTO `t_permission` VALUES ('29', '员工管理', '员工管理', '/page/end/staff.html', 'staff');
INSERT INTO `t_permission` VALUES ('30', '充值管理', '充值管理', '/page/end/charge.html', 'charge');
INSERT INTO `t_permission` VALUES ('31', '服务订单管理', '服务订单管理', '/page/end/buyService.html', 'buyService');
INSERT INTO `t_permission` VALUES ('32', '商品订单管理', '商品订单管理', '/page/end/buyProduct.html', 'buyProduct');

-- ----------------------------
-- Table structure for `t_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `permission` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '权限列表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '超级管理员', '所有权限', '[{\"id\":1,\"name\":\"用户管理\",\"path\":\"/page/end/user.html\",\"description\":null,\"flag\":\"user\"},{\"id\":2,\"name\":\"角色管理\",\"path\":\"/page/end/role.html\",\"description\":null,\"flag\":\"role\"},{\"id\":3,\"name\":\"权限管理\",\"path\":\"/page/end/permission.html\",\"description\":null,\"flag\":\"permission\"},{\"id\":26,\"name\":\"VIP管理\",\"path\":\"/page/end/vIP.html\",\"description\":\"VIP管理\",\"flag\":\"VIP\"},{\"id\":27,\"name\":\"商品管理\",\"path\":\"/page/end/product.html\",\"description\":\"商品管理\",\"flag\":\"product\"},{\"id\":28,\"name\":\"服务项目管理\",\"path\":\"/page/end/service.html\",\"description\":\"服务项目管理\",\"flag\":\"MyService\"},{\"id\":29,\"name\":\"员工管理\",\"path\":\"/page/end/staff.html\",\"description\":\"员工管理\",\"flag\":\"staff\"},{\"id\":30,\"name\":\"充值管理\",\"path\":\"/page/end/charge.html\",\"description\":\"充值管理\",\"flag\":\"charge\"},{\"id\":31,\"name\":\"服务订单管理\",\"path\":\"/page/end/buyService.html\",\"description\":\"服务订单管理\",\"flag\":\"buyService\"},{\"id\":32,\"name\":\"商品订单管理\",\"path\":\"/page/end/buyProduct.html\",\"description\":\"商品订单管理\",\"flag\":\"buyProduct\"}]');
INSERT INTO `t_role` VALUES ('2', '普通用户', '部分权限', '[{\"id\":18,\"name\":\"聊天室\",\"path\":\"/page/end/im.html\",\"description\":\"聊天室\",\"flag\":\"im\"}]');

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像',
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '权限',
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '部门',
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '职位',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', 'admin', '111124444', '13978786565', '1616817714347', '[{\"id\":1,\"name\":\"超级管理员\",\"description\":null,\"permission\":null}]', null, null);
INSERT INTO `t_user` VALUES ('11', 'tom', '123456', 'tom@qq.com', '13685249632', '1616271650817', '[{\"id\":2,\"name\":\"普通用户\",\"description\":\"部分权限\",\"permission\":[{\"id\":18,\"name\":\"聊天室\",\"path\":\"/page/end/im.html\",\"description\":\"聊天室\",\"flag\":\"im\"}]}]', null, null);
INSERT INTO `t_user` VALUES ('16', 'jack', '123456', null, null, '1616271628207', '[{\"id\":2,\"name\":\"仓库管理员\",\"description\":null,\"permission\":[{\"id\":2,\"name\":\"仓库管理\",\"path\":\"/page/end/cangku.html\",\"description\":\"仓库管理\"}]}]', null, null);
INSERT INTO `t_user` VALUES ('20', 'jerry', '123456', 'jerry@qq.com', '13698597854', '1616284768677', '[{\"id\":2,\"name\":\"仓库管理员\",\"description\":null,\"permission\":[{\"id\":4,\"name\":\"部门管理\",\"path\":\"/page/end/department.html\",\"description\":\"部门管理\"},{\"id\":8,\"name\":\"聊天室\",\"path\":\"/page/end/im.html\",\"description\":\"聊天室\"}]}]', null, null);
INSERT INTO `t_user` VALUES ('21', 'hello', '123456', 'hello@qq.com', '13695285412', '1615969390812', '[{\"id\":2,\"name\":\"普通用户\",\"description\":\"部分权限\",\"permission\":[{\"id\":4,\"name\":\"插件管理\",\"path\":\"/page/end/plugins.html\",\"description\":\"插件管理\",\"flag\":\"plugins\"},{\"id\":5,\"name\":\"聊天室\",\"path\":\"/page/end/im.html\",\"description\":\"聊天室\",\"flag\":\"im\"}]}]', null, null);

-- ----------------------------
-- Table structure for `vip`
-- ----------------------------
DROP TABLE IF EXISTS `vip`;
CREATE TABLE `vip` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `gender` tinyint(1) DEFAULT '1' COMMENT '性别',
  `number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电话号码',
  `wallet` int NOT NULL DEFAULT '0' COMMENT '钱包',
  `point` int NOT NULL DEFAULT '0' COMMENT '积分',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of vip
-- ----------------------------
INSERT INTO `vip` VALUES ('1', '小五', '1', '13833333333', '273', '15');
INSERT INTO `vip` VALUES ('2', '迅迅', '1', '13022222222', '365', '48');
INSERT INTO `vip` VALUES ('3', '苏吉拉', '1', '13711111111', '-37', '49');
INSERT INTO `vip` VALUES ('4', '平平', '1', '15600000000', '217', '34');

-- ----------------------------
-- View structure for `vbuyproduct`
-- ----------------------------
DROP VIEW IF EXISTS `vbuyproduct`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbuyproduct` AS select `b`.`id` AS `商品订单号`,`v`.`name` AS `用户`,`p`.`name` AS `商品`,`b`.`time` AS `时间` from ((`buyproduct` `b` join `vip` `v`) join `product` `p`) where ((`b`.`v_id` = `v`.`id`) and (`b`.`p_id` = `p`.`id`)) ;

-- ----------------------------
-- View structure for `vbuyservice`
-- ----------------------------
DROP VIEW IF EXISTS `vbuyservice`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbuyservice` AS select `b`.`id` AS `服务订单号`,`v`.`name` AS `用户`,`s`.`name` AS `项目`,`b`.`time` AS `时间` from ((`buyservice` `b` join `vip` `v`) join `service` `s`) where ((`b`.`v_id` = `v`.`id`) and (`b`.`s_id` = `s`.`id`)) ;

-- ----------------------------
-- View structure for `v_charge`
-- ----------------------------
DROP VIEW IF EXISTS `v_charge`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_charge` AS select `c`.`id` AS `id`,`c`.`vid` AS `v_id`,`v`.`name` AS `name`,`c`.`money` AS `money`,`c`.`time` AS `time` from (`vip` `v` join `charge` `c`) where (`c`.`vid` = `v`.`id`) ;
DROP TRIGGER IF EXISTS `tBuyProduct`;
DELIMITER ;;
CREATE TRIGGER `tBuyProduct` AFTER INSERT ON `buyproduct` FOR EACH ROW begin 
	UPDATE vip v,product p set v.wallet=v.wallet-p.price, v.point=v.point+p.point
	where v.id = new.v_id and p.id=new.p_id;
end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `tBuyService`;
DELIMITER ;;
CREATE TRIGGER `tBuyService` AFTER INSERT ON `buyservice` FOR EACH ROW begin 
	UPDATE vip v,service s set v.wallet=v.wallet-s.price, v.point=v.point+s.point
	where v.id = new.v_id and s.id=new.s_id;
end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_charge`;
DELIMITER ;;
CREATE TRIGGER `trigger_charge` AFTER INSERT ON `charge` FOR EACH ROW begin 
	update vip v set v.wallet =v.wallet + new.money
	where v.id = new.vid;
end
;;
DELIMITER ;
