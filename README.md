# 软件设计课设
_iBarber理发店会员管理系统  2021.6_

## 🛫介绍

这是我们大三下学期软件设计课设做的一个项目，主要参考了Gitee上一位大牛的项目[x-admin: 毕设脚手架工程，拿来即用，简单便捷 (gitee.com)](https://gitee.com/xqnode/x-admin?_from=gitee_search)，结合了**单例模式、外观模式、观察者模式以及责任链模式**，最终完成一个理发店会员管理系统。

主要功能包括数据管理页面，以及对一些表的CURD操作。

## 🌋软件架构

Java后台：SpringBoot 2.1.0 + Mybatis-plus+ hutool工具包 + Apache poi

前台页面：Vue2.0 + ElementUI + Jquery + tinymce（富文本插件）

数据库：Mysql5.7+

前后端分离，页面可单独部署，默认放在项目的static文件夹，随后端工程一起访问。

## 🛸使用教程

1. 使用IDEA打开文件夹
2. 等待IDEA自动下载依赖，建立索引
3. 创建数据库，执行test.sql文件
4. 修改application.yml中的数据库密码
5. 直接运行 Application 启动SpringBoot即可

## 🪐使用说明

1. 登录页面请访问：http://localhost:9999/page/end/login.html
2. 账号：admin，密码：admin





